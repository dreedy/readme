# Dallas’s Personal README

Hi! 👋 This personal README is meant as a bit of a guide to who I am, how I think, and how I work.

This README is intended to serve anyone that works directly with me at GitLab. This is not meant to be a static file, & its contents will change as I learn & grow in my role at GitLab. I hope it provides you with some useful insight or inspiration.

* [🦊 My GitLab Handle](https://gitlab.com/dreedy/)
* [⚽️ Team Page](https://about.gitlab.com/company/team/#dreedy)
* [🐶 Team Buddy](https://about.gitlab.com/company/team-pets/#305-ruby)

## About me

My name is Dallas Reedy and I’m a Fullstack Engineer in the Growth:Conversion group at GitLab.

* I live in [College Place, WA, USA](http://www.cpwa.us/). Our little town is immediately adjacent to [Walla Walla, WA](https://www.wallawallawa.gov/), known for [sweet onions](https://www.sweetonions.org/), [wine](https://www.wallawalla.org/wineries/), & the [penitentiary](https://www.doc.wa.gov/corrections/incarceration/prisons/wsp.htm). My timezone is [PDT](https://time.is/PDT).
* I was born in & grew up just outside of [Portland, OR, USA](https://www.portlandoregon.gov/), about a 3.5-hour drive from College Place.
* My wife, Nicole, & I were married in 2008 & in lieu of children (so far) we have an adorable Pekingese-Chihuahua-Rat-Terrier dog named [Ruby](https://about.gitlab.com/company/team-pets/#305-ruby). She was born & re-homed into our family in the summer of 2012.
* I am a [Seventh-day Adventist (SDA) Christian](https://www.adventist.org/). As such, I rarely work on Friday afternoons & never during [the Sabbath](https://www.adventist.org/beliefs/fundamental-beliefs/living/the-sabbath/) (sundown Friday to sundown Saturday).
* I was part owner of 16 Keys (permanently closed), an [escape room](https://en.wikipedia.org/wiki/Escape_room) in our little town for 4 years.
* I consider myself to be a [“Digital Generalist”](https://www.amazon.com/dp/B07H1ZYWTM/) – a jack-of-all-trades, master of none (or, at least, _very few_).
* I’m an [“ambivert”](https://www.quietrev.com/ambivert-results/) – 55% introverted, 45% extroverted. I meander between the two constantly depending on the situation & my energy level.

### Personal principles

1. Be patient & kind
1. Be humble
1. Be friendly & warm; inviting & inclusive
1. Be the first to apologize & mean it
1. Words & punctuation matter; people matter more
1. Be open to criticism, critique, feedback, & new ideas
1. Welcome respectful, constructive debate & disagreement
1. Don’t be afraid of being wrong or of asking too many questions; this is how you learn & grow

## Logistics

### Work hours

My scheduled “in office” hours are outlined in the table below (all times are PST/PDT):

![my scheduled work time: Monday through Friday, 7am to 12pm. Monday, Wednesday, & Thursday, 2pm to 5:30pm. Tuesday 1pm to 5:30pm.](img/Availability%20Calendar.png)

You can find my fully up-to-date schedule on my GitLab Google Calendar.

### Scheduling

I have a Calendly that you can use to schedule a 25-minute meeting with me. If you’re a fellow GitLab Team Member, feel free to plop anything onto my calendar directly provided that I don’t have anything else already in that spot & it is generally within the above “work hours” schedule (I’m typically flexible an hour before or after those time blocks if need be).

### How to reach me

* **GitLab:** I use email for my GitLab to-dos. Please `@` me directly in comments so I see your message.
* **Email:** I read my email multiple times a day. You should not feel obligated to read or respond to any email you receive from me outside of your normal work hours.
* **Slack:** DMing or `@`-mentioning is probably the best way to reach me during the day. I do not always have Slack open during business hours, but I do check it at least twice a day. I do not have the GitLab Slack account on my personal phone, so I will not respond during non-work time.

## Communication style

When I’m really excited about something I tend to talk a lot about it with gusto. Feel free to ask me to slow down, to quiet down, or to be silent altogether. I also tend to gesticulate with my hands, head, & face. I’ve been told that I make funny faces while talking & listening. Unfortunately, my mind often wanders off in the middle of sentences (that I read or that others speak) & I have to force myself to remain in the moment & keep focused. I apologize in advance for all the times that I’ll find myself only half-listening to what you’re saying. It’s not you, it’s me!

## Footnotes

This document is a constant work in progress. I would love your feedback! Did you find the time you spent reading this valuable? Was something critical missing? Feel free to go ahead & submit an MR to this document.

If you see me not living up to anything included please let me know :raising_hand:. It is possible that I have changed my mind, or that I am just dropping the ball. Either way, get in touch!

---

## Inspiration

Some projects which inspired me to create this one (in no particular order):

* [Rayana Verissimo / 📖 README](https://gitlab.com/rayana/readme/-/blob/master/README.md)
* [Matej Latin / Focus](https://gitlab.com/matejlatin/focus/-/blob/master/README.md)
* [Tim Hey's README](https://about.gitlab.com/handbook/product/readme/tim-hey.html)
